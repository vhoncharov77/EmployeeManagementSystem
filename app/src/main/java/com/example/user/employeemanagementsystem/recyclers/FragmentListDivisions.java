package com.example.user.employeemanagementsystem.recyclers;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.employeemanagementsystem.R;
import com.example.user.employeemanagementsystem.api.provider.DivisionsProvider;

import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentListDivisions extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;
    private RecyclerViewAdapterDivision adapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public RecyclerViewAdapterDivision getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerViewAdapterDivision adapter) {
        this.adapter = adapter;
    }

    public View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragmen_list_divisions, container, false);

        Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.where(Division.class).findAll().deleteAllFromRealm();
            }
        });

        DivisionsProvider divisionsProvider = new DivisionsProvider();
        Observable<List<Division>> divisionsObservable = divisionsProvider.getDivisions();
        divisionsObservable.subscribe(divisions -> {
            adapter.setListDivision(divisions);
        });


        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container_div);
        mSwipeRefreshLayout.setOnRefreshListener(this);


        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        recyclerView = v.findViewById(R.id.recycler_dv);
        adapter = new RecyclerViewAdapterDivision(Realm.getDefaultInstance().where(Division.class).findAll(), getActivity());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());

        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(adapter);

        return v;

    }

    @Override
    public void onRefresh() {
        // TODO Auto-generated method stub
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                adapter.refreshData();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 10);
    }

}

