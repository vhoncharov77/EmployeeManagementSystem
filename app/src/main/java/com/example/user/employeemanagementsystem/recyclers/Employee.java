package com.example.user.employeemanagementsystem.recyclers;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by USER on 1/5/2018.
 */

public class Employee extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    private long id;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("salary")
    private String salary;
    @SerializedName("birthdate")
    private Date birthdate;
    @SerializedName("active")
    private boolean active;
    @SerializedName("division_id")
    private long division;
    @SerializedName("division_name")
    private String divisionname;

    public Employee() {
    }

    public Employee(long id, String firstName, String lastName, String salary, Date birthdate, boolean active, long division, String divisionname) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
        this.birthdate = birthdate;
        this.active = active;
        this.division = division;
        this.divisionname = divisionname;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getDivision() {
        return division;
    }

    public void setDivision(long division) {
        this.division = division;
    }

    public String getDivisionname() {
        return divisionname;
    }

    public void setDivisionname(String divisionname) {
        this.divisionname = divisionname;
    }
}
