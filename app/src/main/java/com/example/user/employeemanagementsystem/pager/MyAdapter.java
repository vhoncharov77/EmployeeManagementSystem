package com.example.user.employeemanagementsystem.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.user.employeemanagementsystem.recyclers.FragmentListDivisions;
import com.example.user.employeemanagementsystem.recyclers.FragmentListEmployees;

/**
 * Created by USER on 1/2/2018.
 */


public class MyAdapter extends FragmentStatePagerAdapter {

    private int tabCount;
    private FragmentListEmployees fragmentListEmployees = new FragmentListEmployees();
    private FragmentListDivisions fragmentListDivisions = new FragmentListDivisions();


    public MyAdapter(FragmentManager mgr, int tabCount) {
        super(mgr);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 1:
                return fragmentListDivisions;
            case 0:
                return fragmentListEmployees;
            default:
                return fragmentListEmployees;
        }
    }

    public FragmentListEmployees getFragmentListEmployees() {
        return fragmentListEmployees;
    }

    public void setFragmentListEmployees(FragmentListEmployees fragmentListEmployees) {
        this.fragmentListEmployees = fragmentListEmployees;
    }

    public FragmentListDivisions getFragmentListDivisions() {
        return fragmentListDivisions;
    }

    public void setFragmentListDivisions(FragmentListDivisions fragmentListDivisions) {
        this.fragmentListDivisions = fragmentListDivisions;
    }

    @Override
    public int getCount() {
        return tabCount;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 1:
                return "Data input";
            case 0:
                return "View data";
            default:
                return "View data";
        }
    }
}


