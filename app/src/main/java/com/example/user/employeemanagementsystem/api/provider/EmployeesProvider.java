package com.example.user.employeemanagementsystem.api.provider;

import com.example.user.employeemanagementsystem.App;
import com.example.user.employeemanagementsystem.User;
import com.example.user.employeemanagementsystem.api.result.DeleteResult;
import com.example.user.employeemanagementsystem.api.result.EmployeeResult;
import com.example.user.employeemanagementsystem.api.result.EmployeesResult;
import com.example.user.employeemanagementsystem.recyclers.Employee;

import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 1/10/2018.
 */

public class EmployeesProvider {

    public Observable<List<Employee>> getEmployees(boolean active, boolean inactive, String first_name, String last_name, Integer division_id) {
        return Observable.create(emitter -> {
            App.getApi().getEmployees(Realm.getDefaultInstance().where(User.class).findFirst().getToken(), active, inactive, first_name, last_name, division_id).enqueue(new Callback<EmployeesResult>() {
                @Override
                public void onResponse(Call<EmployeesResult> call, Response<EmployeesResult> response) {
                    if (response.isSuccessful()) {
                        emitter.onNext(response.body().getEmployees());


                    }
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<EmployeesResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });
        });
    }

    public Observable<Employee> editEmployee(long id, boolean active,  String first_name, String last_name, String birthdate, String salary, Integer division_id) {
        return Observable.create(emitter -> {
            App.getApi().editEmployee(Realm.getDefaultInstance().where(User.class).findFirst().getToken(), id,  active, first_name, last_name, birthdate, salary, division_id).enqueue(new Callback<EmployeeResult>() {
                @Override
                public void onResponse(Call<EmployeeResult> call, Response<EmployeeResult> response) {
                    if (response.isSuccessful()) {
                        emitter.onNext(response.body().getEmployee());


                    }
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<EmployeeResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });
        });
    }

    public Observable<Employee> postEmployee(String first_name, String last_name, String birthdate, String salary, Integer division_id) {
        return Observable.create(emitter -> {
            App.getApi().postEmployee(Realm.getDefaultInstance().where(User.class).findFirst().getToken(), first_name, last_name, birthdate, salary, division_id).enqueue(new Callback<EmployeeResult>() {
                @Override
                public void onResponse(Call<EmployeeResult> call, Response<EmployeeResult> response) {
                    System.out.println(response);
                    if (response.isSuccessful()) {
                        emitter.onNext(response.body().getEmployee());


                    }
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<EmployeeResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });
        });
    }

    public Observable<Boolean> deleteEmployee(long id) {
        return Observable.create(emitter -> {
            App.getApi().deleteEmployee(Realm.getDefaultInstance().where(User.class).findFirst().getToken(),id).enqueue(new Callback<DeleteResult>() {
                @Override
                public void onResponse(Call<DeleteResult> call, Response<DeleteResult> response) {
                    System.out.println(response);
                    if (response.isSuccessful()) {
                        emitter.onNext(response.body().isDelete_result());


                    }
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<DeleteResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });
        });
    }



}

