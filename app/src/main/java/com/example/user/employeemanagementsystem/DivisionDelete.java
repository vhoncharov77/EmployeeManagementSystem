package com.example.user.employeemanagementsystem;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.employeemanagementsystem.api.provider.DivisionsProvider;
import com.example.user.employeemanagementsystem.recyclers.Division;
import com.example.user.employeemanagementsystem.recyclers.RecyclerViewAdapterDivision;

import io.reactivex.Observable;
import io.realm.Realm;

public class DivisionDelete extends DialogFragment implements OnClickListener {

    private TextView deletDivision;
    private TextView titleDeletDivision;
    private int id = 0;
    private Button divDelOkBtn;
    private Button divDelCancellBtn;

    private RecyclerViewAdapterDivision adapter;
    private int position;

    final String LOG_TAG = "myLogs";

    private Division division = new Division();

    public void setDivision(Division division) {
        this.division = division;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_delete_employee, null);
        divDelOkBtn = v.findViewById(R.id.delete_btnYes);
        divDelCancellBtn = v.findViewById(R.id.delete_btnNo);
        divDelOkBtn.setOnClickListener(this);


        deletDivision = (TextView) v.findViewById(R.id.delete_text);
        titleDeletDivision = (TextView) v.findViewById(R.id.delete_title);
        titleDeletDivision.setText("Division delete");

        divDelOkBtn.setText("Delete division");
        deletDivision.setText("You really want to delete  division - " + division.getName());


        divDelCancellBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return v;
    }


    public void onClick(View v) {
        id = (int) division.getId();

        DivisionsProvider divProvider = new DivisionsProvider();
        Observable<Boolean> divisionObservable = divProvider.deleteDivision(id);
        divisionObservable.subscribe(Boolean -> {
            adapter.remove(position);
            Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.where(Division.class).equalTo("id", id).findFirst().deleteFromRealm();
                }
            });
        });
        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }

    public RecyclerViewAdapterDivision getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerViewAdapterDivision adapter) {
        this.adapter = adapter;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
