package com.example.user.employeemanagementsystem.api.result;

import com.example.user.employeemanagementsystem.recyclers.Division;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 1/10/2018.
 */

public class DivisionsResult {
    @SerializedName("divisions")
    private List<Division> divisions;

    public List<Division> getDivisions() {
        return divisions;
    }

    public void setDivisions(List<Division> divisions) {
        this.divisions = divisions;
    }
}
