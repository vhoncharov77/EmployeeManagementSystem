package com.example.user.employeemanagementsystem;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.user.employeemanagementsystem.api.provider.UserProvider;
import com.example.user.employeemanagementsystem.pager.ActivityPageView;

import io.reactivex.Observable;

public class ActivityLogin extends AppCompatActivity {
    private EditText loginInput;
    private EditText passInput;
    private Button myButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginInput = (EditText) findViewById(R.id.aut_login);
        passInput = (EditText) findViewById(R.id.aut_pass);
        myButton = findViewById(R.id.my_button);


        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                UserProvider userProvider = new UserProvider(ActivityLogin.this);
                Observable<User> userObservable = userProvider.getUser(loginInput.getText().toString(), passInput.getText().toString(), getDeviceId());
                userObservable.subscribe(user -> {

                    Intent intent = new Intent(ActivityLogin.this, ActivityPageView.class);
                    intent.putExtra("token", user.getToken());
                    startActivity(intent);

                });

            }
        });
    }


    public String getDeviceId() {
        return Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}

