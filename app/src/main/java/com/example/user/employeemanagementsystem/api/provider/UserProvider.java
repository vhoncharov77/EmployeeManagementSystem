package com.example.user.employeemanagementsystem.api.provider;

import android.content.Context;
import android.widget.Toast;

import com.example.user.employeemanagementsystem.App;
import com.example.user.employeemanagementsystem.User;
import com.example.user.employeemanagementsystem.api.result.UserResult;

import io.reactivex.Observable;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by USER on 1/17/2018.
 */

public class UserProvider {
    private Context context;

    public UserProvider(Context context) {
        this.context = context;
    }

    public Observable<User> getUser(String username, String password, String deviceId) {
        return Observable.create(emitter -> {
            App.getApi().login(username, password, deviceId).enqueue(new Callback<UserResult>() {

                @Override
                public void onResponse(Call<UserResult> call, Response<UserResult> response) {
                    if (response.isSuccessful()) {
                        emitter.onNext(response.body().getUser());

                        User user = response.body().getUser();
                        user.setId(0);
                        Toast.makeText(context, "Login OK", Toast.LENGTH_SHORT).show();
                        Realm.getDefaultInstance().executeTransaction(realm -> {
                            realm.copyToRealmOrUpdate(user);
                        });

                    } else { Toast.makeText(context, "Login or password not found.  Please try again.", Toast.LENGTH_SHORT).show();}
                    emitter.onComplete();
                }
                @Override
                public void onFailure(Call<UserResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });
        });
    }
}