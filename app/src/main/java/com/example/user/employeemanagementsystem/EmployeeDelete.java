package com.example.user.employeemanagementsystem;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.employeemanagementsystem.api.provider.EmployeesProvider;
import com.example.user.employeemanagementsystem.recyclers.Employee;
import com.example.user.employeemanagementsystem.recyclers.RecyclerViewAdapter;

import io.reactivex.Observable;

public class EmployeeDelete extends DialogFragment implements OnClickListener {

    private TextView employeeDelet;
    private TextView titleDelet;
    private Button okBtn;
    private Button cancellBtn;
    private int position;
    private RecyclerViewAdapter adapter;
    final String LOG_TAG = "myLogs";
    private Employee employee = new Employee();

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_delete_employee, null);
        okBtn = v.findViewById(R.id.delete_btnYes);
        cancellBtn = v.findViewById(R.id.delete_btnNo);
        employeeDelet = v.findViewById(R.id.delete_text);
        titleDelet = v.findViewById(R.id.delete_title);
        titleDelet.setText("Employee delete");
        okBtn.setOnClickListener(this);
        employeeDelet.setText("You really want to delete  employee - " + employee.getFirstName() + " " + employee.getLastName() + " from division " + employee.getDivisionname());

        cancellBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return v;
    }


    public void onClick(View v) {

        EmployeesProvider employeesProvider = new EmployeesProvider();
        Observable<Boolean> deleteObservable = employeesProvider.deleteEmployee(employee.getId());
        deleteObservable.subscribe(Boolean -> {
            adapter.remove(position);
        });
        dismiss();
//        if (getActivity() != null) { ActivityPageView ma = (ActivityPageView ) getActivity(); ma.refreshEmployee(); }

        Toast.makeText(getContext(), "Yemployee - " + employee.getFirstName() + " " + employee.getLastName() + " deleted", Toast.LENGTH_SHORT).show();

        dismiss();
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }

    public RecyclerViewAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerViewAdapter adapter) {
        this.adapter = adapter;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
