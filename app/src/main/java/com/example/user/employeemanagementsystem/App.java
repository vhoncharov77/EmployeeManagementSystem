package com.example.user.employeemanagementsystem;

import android.app.Application;

import com.example.user.employeemanagementsystem.api.Api;

import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by USER on 1/11/2018.
 */

public class App extends Application {

    private final static int TIMEOUT = 30;
    private static Api api;


    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }

    public static Api getApi() {
        if (api == null) {
            api = new Retrofit.Builder()
                    .baseUrl("http://10.11.1.164:8080")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(createOkHttpClient())
                    .build()
                    .create(Api.class);
        }
        return api;
    }

    private static OkHttpClient createOkHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .build();
    }


}
