package com.example.user.employeemanagementsystem.recyclers;


import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.employeemanagementsystem.EmployeeDelete;
import com.example.user.employeemanagementsystem.EmployeeFullView;
import com.example.user.employeemanagementsystem.R;
import com.example.user.employeemanagementsystem.api.provider.EmployeesProvider;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by USER on 1/5/2018.
 */


public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {
    private List<Employee> listEmployee = new ArrayList<Employee>();

    private FragmentActivity fragmentActivity;


    public RecyclerViewAdapter(FragmentActivity fragmentActivity) {
        this.fragmentActivity = fragmentActivity;
    }

    public void remove(int position) {
        listEmployee.remove(position);
        notifyItemRemoved(position);
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView txtName;
        public TextView txtLastName;
        public TextView txtSalary;
        public TextView txtDivision;
        public CardView cardView;

        public TextView fullName;
        public TextView fullLastName;
        public TextView fullSalary;
        public TextView fullDivision;
        public TextView fullBirthDate;
        public TextView fullActive;


        public RecyclerViewHolder(View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imageView1);
            txtName = (TextView) itemView.findViewById(R.id.my_textview1);
            txtLastName = (TextView) itemView.findViewById(R.id.my_textview2);
            txtSalary = (TextView) itemView.findViewById(R.id.my_textview3);
            txtDivision = (TextView) itemView.findViewById(R.id.my_devision);
            cardView = itemView.findViewById(R.id.cv);
            fullLastName = (TextView) itemView.findViewById(R.id.full_last_name);
            fullName = (TextView) itemView.findViewById(R.id.full_first_name);
            fullSalary = (TextView) itemView.findViewById(R.id.full_salary);
            fullDivision = (TextView) itemView.findViewById(R.id.full_devision);
            fullBirthDate = (TextView) itemView.findViewById(R.id.full_birthday);
            fullActive = (TextView) itemView.findViewById(R.id.full_active);

        }


        public void onClick(View view) {
            EmployeeFullView dialog = new EmployeeFullView();

        }


    }


    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemViev = inflater.inflate(R.layout.employee_cv, parent, false);
        return new RecyclerViewHolder(itemViev);

    }

    public void refreshData() {
        EmployeesProvider employeesProvider = new EmployeesProvider();
        Observable<List<Employee>> employeesObservable = employeesProvider.getEmployees(true, true, null, null, null);
        employeesObservable.subscribe(employees -> {
            setListEmployee(employees);
            notifyDataSetChanged();
        });

    }

    public void setListEmployee(List<Employee> listEmployee) {
        this.listEmployee = listEmployee;
        notifyDataSetChanged();
    }


    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        int pos = holder.getAdapterPosition();
        Employee employee = listEmployee.get(pos);
        holder.txtName.setText("Name       - " + employee.getFirstName());
        holder.txtLastName.setText("Last name - " + employee.getLastName());
        holder.txtSalary.setText("Salary         - " + employee.getSalary());
        holder.txtDivision.setText("Devision    - " + employee.getDivisionname());

        holder.cardView.setOnClickListener(view -> {

            EmployeeFullView dialog = new EmployeeFullView();
            dialog.setEmployee(employee);
            dialog.setPosition(pos);
            dialog.setAdapter(this);
            dialog.setStyle(EmployeeFullView.STYLE_NO_TITLE, 0);
            dialog.show(fragmentActivity.getSupportFragmentManager(), "dialog");


        });
        holder.cardView.setOnLongClickListener(view -> {
            EmployeeDelete dialog = new EmployeeDelete();
            dialog.setEmployee(employee);
            dialog.setPosition(pos);
            dialog.setAdapter(this);
            dialog.setStyle(EmployeeDelete.STYLE_NO_TITLE, 0);
            dialog.show(fragmentActivity.getSupportFragmentManager(), "dialog_delete");
            return true;
        });

    }


    @Override
    public int getItemCount() {
        return listEmployee.size();
    }


}