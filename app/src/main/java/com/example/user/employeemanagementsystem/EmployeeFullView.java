package com.example.user.employeemanagementsystem;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.user.employeemanagementsystem.recyclers.Employee;
import com.example.user.employeemanagementsystem.recyclers.RecyclerViewAdapter;

import java.text.SimpleDateFormat;

public class EmployeeFullView extends DialogFragment implements OnClickListener {

    private Employee employee = new Employee();
    private int position;
    private RecyclerViewAdapter adapter;
    private TextView fullName;
    private TextView fullLastName;
    private TextView fullSalary;
    private TextView fullDivision;
    private TextView fullBirthDate;
    private TextView fullActive;
    private TextView fullTitle;
    final String LOG_TAG = "myLogs";

    public void setAdapter(RecyclerViewAdapter adapter) {
        this.adapter = adapter;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.dialog_eployee_full_view, null);
        v.findViewById(R.id.btnYes).setOnClickListener(this);
        Button editBtn = (Button) v.findViewById(R.id.full_btn_edit);
        fullTitle = (TextView) v.findViewById(R.id.full_title);
        fullTitle.setText("Employee - ");
        fullLastName = (TextView) v.findViewById(R.id.full_last_name);
        fullName = (TextView) v.findViewById(R.id.full_first_name);
        fullSalary = (TextView) v.findViewById(R.id.full_salary);
        fullDivision = (TextView) v.findViewById(R.id.full_devision);
        fullBirthDate = (TextView) v.findViewById(R.id.full_birthday);
        fullActive = (TextView) v.findViewById(R.id.full_active);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        fullName.setText("Name - " + employee.getFirstName());
        fullLastName.setText("Last name - " + employee.getLastName());
        fullSalary.setText("Salary - " + employee.getSalary());
        fullDivision.setText("Division - " + employee.getDivisionname());
        fullBirthDate.setText("Birth date - " + formatter.format(employee.getBirthdate()));
        fullActive.setText("Active" + employee.isActive());


        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EmployeeEdit dialog = new EmployeeEdit();
                dialog.setEmployee(employee);
                dialog.setPosition(position);
                dialog.setAdapter(adapter);
                dialog.setStyle(EmployeeDelete.STYLE_NO_TITLE, 0);
                dialog.show(getActivity().getSupportFragmentManager(), "dialog_edit");
                dismiss();
            }
        });


        return v;
    }

    public void onClick(View v) {
        Log.d(LOG_TAG, "Dialog 1: " + ((Button) v).getText());
        dismiss();
    }


    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }


}
