package com.example.user.employeemanagementsystem;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class DatePicker extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {
    private String date;
    private TextView dateText;

    public void setDate(String date) {
        this.date = date;
    }

    public void setTextView(TextView dateText) {
        this.dateText = dateText;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // определяем текущую дату
        final Calendar c = Calendar.getInstance();
        int year = Integer.parseInt(date.substring(0, 4));
        int month = (Integer.parseInt(date.substring(5, 7))) - 1;
        int day = Integer.parseInt(date.substring(8));

        // создаем DatePickerDialog и возвращаем его
        DatePickerDialog picker = new DatePickerDialog(getActivity(), this,
                year, month, day);
        picker.setTitle(getResources().getString(R.string.choose_date));

        return picker;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int year,
                          int month, int day) {
        Toast.makeText(getActivity(), "BTN CLICKED", Toast.LENGTH_SHORT).show();
        String monthOut;
        String dayOut;
        if (++month < 10) {
            monthOut = "0" + (month) + "-";
        } else {
            monthOut = +(month) + "-";
        }
        if (day < 10) {
            dayOut = "0" + day + "-";
        } else {
            dayOut = day + "-";
        }

        dateText.setText(dayOut + monthOut + year);
    }

}