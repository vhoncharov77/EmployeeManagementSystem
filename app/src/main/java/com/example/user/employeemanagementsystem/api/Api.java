package com.example.user.employeemanagementsystem.api;

import com.example.user.employeemanagementsystem.api.result.DeleteResult;
import com.example.user.employeemanagementsystem.api.result.DivisionResult;
import com.example.user.employeemanagementsystem.api.result.DivisionsResult;
import com.example.user.employeemanagementsystem.api.result.EmployeeResult;
import com.example.user.employeemanagementsystem.api.result.EmployeesResult;
import com.example.user.employeemanagementsystem.api.result.UserResult;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;

/**
 * Created by USER on 1/11/2018.
 */

public interface Api {

    @GET("./employee")
    Call<EmployeesResult> getEmployees(@Header("Auth-token") String token,
                                       @Query("active") boolean active,
                                       @Query("inactive") boolean inactive,
                                       @Query("first_name") String first_name,
                                       @Query("last_name") String last_name,
                                       @Query("division_id") Integer division_id);


    @PUT("./employee")
    Call<EmployeeResult> editEmployee(@Header("Auth-token") String token,
                                      @Query("id") long id,
                                      @Query("active") boolean active,
                                      @Query("first_name") String first_name,
                                      @Query("last_name") String last_name,
                                      @Query("birthdate") String birthdate,
                                      @Query("salary") String salary,
                                      @Query("division_id") Integer division_id);

    @POST("./employee")
    Call<EmployeeResult> postEmployee(@Header("Auth-token") String token,
                                      @Query("first_name") String first_name,
                                      @Query("last_name") String last_name,
                                      @Query("salary") String salary,
                                      @Query("birthdate") String birthdate,
                                      @Query("division_id") Integer division_id);

    @DELETE("./employee")
    Call<DeleteResult> deleteEmployee(@Header("Auth-token") String token,
                                      @Query("id") long id );


    @GET("./division")
    Call<DivisionsResult> getDivisions(@Header("Auth-token") String token);

    @PUT("./division")
    Call<DivisionResult> editDivision(@Header("Auth-token") String token,
                                     @Query("id") int id,
                                     @Query("name") String name);

    @POST("./division")
    Call<DivisionResult> postDivision(@Header("Auth-token") String token,
                                      @Query("name") String name);

    @DELETE("./division")
    Call<DeleteResult> deleteDivision(@Header("Auth-token") String token,
                                      @Query("id") int id );



    @POST("./login")
    Call<UserResult> login(@Query("username") String username,
                           @Query("password") String password,
                           @Query("device_id") String device_id);

}
