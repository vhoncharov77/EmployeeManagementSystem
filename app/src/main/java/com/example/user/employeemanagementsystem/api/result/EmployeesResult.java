package com.example.user.employeemanagementsystem.api.result;

import com.example.user.employeemanagementsystem.recyclers.Employee;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 1/10/2018.
 */

public class EmployeesResult {
    @SerializedName("employees")
    private List<Employee> employees;

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
