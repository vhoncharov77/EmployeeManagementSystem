package com.example.user.employeemanagementsystem;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by USER on 1/17/2018.
 */

public class User extends RealmObject {
    @PrimaryKey
    @SerializedName("id")
    private int id;
    @SerializedName("role")
    private String role;
    @SerializedName("username")
    private String username;
    @SerializedName("token")
    private String token;

    public User() {
    }

    public User(int id, String role, String username, String token) {
        this.id = id;
        this.role = role;
        this.username = username;
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
