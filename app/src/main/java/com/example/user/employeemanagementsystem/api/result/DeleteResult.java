package com.example.user.employeemanagementsystem.api.result;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 1/10/2018.
 */

public class DeleteResult {
    @SerializedName("delete_result")
    private boolean delete_result;

    public DeleteResult(boolean delete_result) {
        this.delete_result = delete_result;
    }

    public boolean isDelete_result() {
        return delete_result;
    }

    public void setDelete_result(boolean delete_result) {
        this.delete_result = delete_result;
    }
}
