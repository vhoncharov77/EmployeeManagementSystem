package com.example.user.employeemanagementsystem.api.result;

import com.example.user.employeemanagementsystem.recyclers.Division;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 1/10/2018.
 */

public class DivisionResult {
    @SerializedName("division")
    private Division division;

    public DivisionResult(Division division) {
        this.division = division;
    }

    public Division getDivision() {
        return division;
    }

    public void setDivision(Division division) {
        this.division = division;
    }
}
