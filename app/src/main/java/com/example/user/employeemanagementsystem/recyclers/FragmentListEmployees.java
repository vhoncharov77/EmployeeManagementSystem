package com.example.user.employeemanagementsystem.recyclers;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.employeemanagementsystem.R;
import com.example.user.employeemanagementsystem.api.provider.EmployeesProvider;

import java.util.List;

import io.reactivex.Observable;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentListEmployees extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private RecyclerView recyclerView;
    public RecyclerViewAdapter adapter;

    public RecyclerViewAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerViewAdapter adapter) {
        this.adapter = adapter;
    }

    private SwipeRefreshLayout mSwipeRefreshLayout;


    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragmen_list_employee, container, false);


        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container_employee);
        mSwipeRefreshLayout.setOnRefreshListener(this);


        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        recyclerView = v.findViewById(R.id.recycler_employee);
        adapter = new RecyclerViewAdapter(getActivity());
        EmployeesProvider employeesProvider = new EmployeesProvider();
        Observable<List<Employee>> employeesObservable = employeesProvider.getEmployees(true, true, null, null, null);
        employeesObservable.subscribe(employees -> {
            adapter.setListEmployee(employees);
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());

        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(adapter);


        return v;
    }

    @Override
    public void onRefresh() {
        // TODO Auto-generated method stub
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                adapter.refreshData();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 10);
    }

}

