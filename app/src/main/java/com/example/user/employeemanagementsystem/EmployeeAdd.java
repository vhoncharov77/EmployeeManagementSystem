package com.example.user.employeemanagementsystem;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.employeemanagementsystem.api.provider.EmployeesProvider;
import com.example.user.employeemanagementsystem.recyclers.Division;
import com.example.user.employeemanagementsystem.recyclers.Employee;
import com.example.user.employeemanagementsystem.recyclers.RecyclerViewAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observable;
import io.realm.Realm;

public class EmployeeAdd extends DialogFragment implements OnClickListener {

    private EditText addName;
    private EditText addLastName;
    private TextView titleAdd;
    private EditText addSalary;
    private TextView addDate;
    private CheckBox editActive;
    private CheckBox editInActive;
    private Button addOkBtn;
    private Button addCancellBtn;
    private Button addDatePicker;
    private String first_name = null;
    private String last_name = null;
    private String salary = null;
    private String birthDate;
    private Integer division_id;
    private RecyclerViewAdapter adapter;
    private SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
    final String LOG_TAG = "myLogs";
    private Employee employee = new Employee();

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_add_emloyee, null);
        addOkBtn = v.findViewById(R.id.add_ok);
        addCancellBtn = v.findViewById(R.id.add_cancel);
        addDatePicker = v.findViewById(R.id.add_date_btn);
        addOkBtn.setOnClickListener(this);

        ArrayList<String> spinnerList = new ArrayList<String>();
        List<Division> listDiv = Realm.getDefaultInstance().where(Division.class).findAll();
        for (Division division : listDiv) {
            spinnerList.add(division.getName());
        }

        // адаптер
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, spinnerList);
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = (Spinner) v.findViewById(R.id.add_division);
        spinner.setAdapter(adapter3);
        // заголовок
        spinner.setPrompt("Divisions");
        // выделяем элемент
        spinner.setSelection(spinnerList.indexOf(employee.getDivisionname()));
        // устанавливаем обработчик нажатия
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                String s;
                s = (String) spinner.getSelectedItem();
                division_id = (int) Realm.getDefaultInstance().where(Division.class).equalTo("name", s).findFirst().getId();
                // показываем позиция нажатого элемента
                Toast.makeText(getContext(), "Division = " + division_id, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        addName = (EditText) v.findViewById(R.id.add_first_name);

        titleAdd = (TextView) v.findViewById(R.id.add_title);
        addSalary = (EditText) v.findViewById(R.id.add_salary);
        addDate = (TextView) v.findViewById(R.id.add_tvDate);
        titleAdd.setText("Employee add");
        addLastName = (EditText) v.findViewById(R.id.add_last_name);


        addDate.setText(formatter.format(new Date()));


        addCancellBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        addDatePicker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker dateDialog = new DatePicker();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                dateDialog.setDate(formatter.format(new Date()));
                dateDialog.setTextView(addDate);
                dateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });


        return v;
    }


    public void onClick(View v) {
        first_name = addName.getText().toString();
        last_name = addLastName.getText().toString();
        birthDate = addDate.getText().toString();
        salary = addSalary.getText().toString();

        Pattern pattern = Pattern.compile("[A-Za-zА-Яа-я0-9'-]*");
        Pattern patternNam = Pattern.compile("\\-?\\d+(\\.\\d{0,})?");
        Matcher matchSalary = patternNam.matcher(salary);
        Matcher matchName = pattern.matcher(first_name);
        Matcher matchLastName = pattern.matcher(last_name);
        if (matchName.matches() && matchLastName.matches() && matchSalary.matches()) {
            EmployeesProvider employeesProvider = new EmployeesProvider();
            Observable<Employee> employeeObservable = employeesProvider.postEmployee(first_name, last_name, salary, birthDate, division_id);
            employeeObservable.subscribe(employee -> {
                System.out.println(employee.getFirstName());
                adapter.refreshData();
            });

            dismiss();
        } else {
            Toast.makeText(getContext(), "Wrong input  Please try again.", Toast.LENGTH_SHORT).show();
        }

    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }

    public RecyclerViewAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerViewAdapter adapter) {
        this.adapter = adapter;
    }
}
