package com.example.user.employeemanagementsystem.recyclers;


import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.user.employeemanagementsystem.DivisionDelete;
import com.example.user.employeemanagementsystem.DivisionEdit;
import com.example.user.employeemanagementsystem.EmployeeFullView;
import com.example.user.employeemanagementsystem.R;
import com.example.user.employeemanagementsystem.api.provider.DivisionsProvider;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by USER on 1/5/2018.
 */


public class RecyclerViewAdapterDivision extends RecyclerView.Adapter<RecyclerViewAdapterDivision.RecyclerViewHolder> {
    private List<Division> listDivision = new ArrayList<Division>();

    private FragmentActivity fragmentActivity;

    public void remove(int position) {
        listDivision.remove(position);
        notifyItemRemoved(position);
    }

    public RecyclerViewAdapterDivision(List<Division> listDivision, FragmentActivity fragmentActivity) {
        this.listDivision = listDivision;
        this.fragmentActivity = fragmentActivity;
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {

        public TextView dvName;
        public TextView dvId;
        public CardView cardView;


        public RecyclerViewHolder(View itemView) {
            super(itemView);

            dvName = (TextView) itemView.findViewById(R.id.dv_devision);
            dvId = (TextView) itemView.findViewById(R.id.dv_devision_id);
            cardView = itemView.findViewById(R.id.cv_div);

        }

        public void onClick(View view) {
            EmployeeFullView dialog = new EmployeeFullView();

        }

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemViev = inflater.inflate(R.layout.division_cv, parent, false);
        return new RecyclerViewHolder(itemViev);

    }

    public void refreshData() {
        DivisionsProvider divisionsProvider = new DivisionsProvider();
        Observable<List<Division>> divisionsObservable = divisionsProvider.getDivisions();
        divisionsObservable.subscribe(divisions -> {
            setListDivision(divisions);
            notifyDataSetChanged();
        });

    }

    public void setListDivision(List<Division> listDivision) {
        this.listDivision = listDivision;
        notifyDataSetChanged();
    }


    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        int pos = holder.getAdapterPosition();
        Division division = listDivision.get(pos);
        holder.dvName.setText("Division - " + division.getName());
        holder.dvId.setText("division id - " + division.getId());


        holder.cardView.setOnClickListener(view -> {

            DivisionEdit dialog = new DivisionEdit();
            dialog.setDivision(division);
            dialog.setAdapter(this);
            dialog.setStyle(DivisionEdit.STYLE_NO_TITLE, 0);
            dialog.show(fragmentActivity.getSupportFragmentManager(), "dialog");


        });

        holder.cardView.setOnLongClickListener(view -> {
            DivisionDelete dialog = new DivisionDelete();
            dialog.setDivision(division);
            dialog.setPosition(pos);
            dialog.setAdapter(this);
            dialog.setStyle(DivisionDelete.STYLE_NO_TITLE, 0);
            dialog.show(fragmentActivity.getSupportFragmentManager(), "dialog_delete");
            return false;

        });

    }

    public void setDialog(View view) {
        EmployeeFullView dialog = new EmployeeFullView();


    }

    @Override
    public int getItemCount() {
        return listDivision.size();
    }
}