package com.example.user.employeemanagementsystem.api.provider;

import com.example.user.employeemanagementsystem.App;
import com.example.user.employeemanagementsystem.User;
import com.example.user.employeemanagementsystem.api.result.DeleteResult;
import com.example.user.employeemanagementsystem.api.result.DivisionResult;
import com.example.user.employeemanagementsystem.api.result.DivisionsResult;
import com.example.user.employeemanagementsystem.recyclers.Division;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 1/10/2018.
 */

public class DivisionsProvider {

    public Observable<List<Division>> getDivisions() {
        return Observable.create(emitter -> {
            App.getApi().getDivisions(Realm.getDefaultInstance().where(User.class).findFirst().getToken()).enqueue(new Callback<DivisionsResult>() {
                @Override
                public void onResponse(Call<DivisionsResult> call, Response<DivisionsResult> response) {
                    if (response.isSuccessful()) {
                        emitter.onNext(response.body().getDivisions());
                        List<Division> divisions = new ArrayList<Division>(response.body().getDivisions());
                        Realm.getDefaultInstance().executeTransaction(realm -> {
                            realm.copyToRealmOrUpdate(divisions);
                        });
                    }
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<DivisionsResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });
        });
    }

    public Observable<Division> editDivision(int id, String name) {
        return Observable.create(emitter -> {
            App.getApi().editDivision(Realm.getDefaultInstance().where(User.class).findFirst().getToken(), id, name).enqueue(new Callback<DivisionResult>() {
                @Override
                public void onResponse(Call<DivisionResult> call, Response<DivisionResult> response) {
                    if (response.isSuccessful()) {
                        emitter.onNext(response.body().getDivision());

                    }
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<DivisionResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });
        });
    }

    public Observable<Division> postDivision(String name) {
        return Observable.create(emitter -> {
            App.getApi().postDivision(Realm.getDefaultInstance().where(User.class).findFirst().getToken(), name).enqueue(new Callback<DivisionResult>() {
                @Override
                public void onResponse(Call<DivisionResult> call, Response<DivisionResult> response) {
                    if (response.isSuccessful()) {
                        emitter.onNext(response.body().getDivision());

                    }
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<DivisionResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });
        });
    }

    public Observable<Boolean> deleteDivision(int id) {
        return Observable.create(emitter -> {
            App.getApi().deleteDivision(Realm.getDefaultInstance().where(User.class).findFirst().getToken(), id).enqueue(new Callback<DeleteResult>() {
                @Override
                public void onResponse(Call<DeleteResult> call, Response<DeleteResult> response) {
                    System.out.println(response);
                    if (response.isSuccessful()) {
                        emitter.onNext(response.body().isDelete_result());


                    }
                    emitter.onComplete();
                }

                @Override
                public void onFailure(Call<DeleteResult> call, Throwable t) {
                    emitter.onError(t);
                    emitter.onComplete();
                }
            });
        });
    }


}

