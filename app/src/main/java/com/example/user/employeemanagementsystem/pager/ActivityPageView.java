package com.example.user.employeemanagementsystem.pager;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.user.employeemanagementsystem.DivisionAdd;
import com.example.user.employeemanagementsystem.EmployeeAdd;
import com.example.user.employeemanagementsystem.EmployeeSearch;
import com.example.user.employeemanagementsystem.R;
import com.example.user.employeemanagementsystem.UserSearch;
import com.example.user.employeemanagementsystem.api.provider.EmployeesProvider;
import com.example.user.employeemanagementsystem.recyclers.Employee;
import com.example.user.employeemanagementsystem.recyclers.RecyclerViewAdapter;

import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;

/**
 * Created by USER on 1/2/2018.
 */

public class ActivityPageView extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    //This is our tablayout
    private TabLayout tabLayout;
    private Button createData;
    //This is our viewPager
    private ViewPager viewPager;
    RecyclerViewAdapter adapter1;
    private MyAdapter adapter;
    private UserSearch userSearch = new UserSearch();


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // получим идентификатор выбранного пункта меню
        int id = item.getItemId();


        // Операции для выбранного пункта меню
        switch (id) {
            case R.id.add_employee_menu:
                EmployeeAdd dialog = new EmployeeAdd();
                dialog.setAdapter(adapter.getFragmentListEmployees().getAdapter());
                dialog.setStyle(EmployeeAdd.STYLE_NO_TITLE, 0);
                dialog.show(getSupportFragmentManager(), "dialog");
                return true;
            case R.id.add_division_menu:
                DivisionAdd dialogDiv = new DivisionAdd();
                dialogDiv.setAdapter(adapter.getFragmentListDivisions().getAdapter());
                dialogDiv.setStyle(EmployeeAdd.STYLE_NO_TITLE, 0);
                dialogDiv.show(getSupportFragmentManager(), "dialog_div");
                return true;
            case R.id.refresh_layout_menu_search:
                userSearch = Realm.getDefaultInstance().where(UserSearch.class).findFirst();
                EmployeesProvider employeesProvider = new EmployeesProvider();
                Observable<List<Employee>> employeesObservable = employeesProvider.getEmployees(userSearch.isActive(), userSearch.isInactive(), userSearch.getFirst_name(), userSearch.getLast_name(), userSearch.getDivision_id());
                employeesObservable.subscribe(employees -> {
                    adapter.getFragmentListEmployees().getAdapter().setListEmployee(employees);
                });
                return true;
            case R.id.refresh_layout_menu:
                EmployeesProvider employeesProvider1 = new EmployeesProvider();
                Observable<List<Employee>> employeesObservable1 = employeesProvider1.getEmployees(true, true, null, null, null);
                employeesObservable1.subscribe(employees -> {
                    adapter.getFragmentListEmployees().getAdapter().setListEmployee(employees);
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.page_view);


        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        //Adding the tabs using addTab() method
        tabLayout.addTab(tabLayout.newTab().setText("Employees"));
        tabLayout.addTab(tabLayout.newTab().setText("Divisions"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initializing viewPager
        viewPager = (ViewPager) findViewById(R.id.pager);

        //Creating our pager adapter
        adapter = new MyAdapter(getSupportFragmentManager(), tabLayout.getTabCount());

        //Adding adapter to pager
        viewPager.setAdapter(adapter);

        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(this);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPageView.this);
                EmployeeSearch dialog = new EmployeeSearch();
                dialog.setStyle(EmployeeSearch.STYLE_NO_TITLE, 0);
                dialog.setAdapter(adapter.getFragmentListEmployees().getAdapter());
                dialog.show(ActivityPageView.this.getSupportFragmentManager(), "dialog_search");
            }
        });
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

}