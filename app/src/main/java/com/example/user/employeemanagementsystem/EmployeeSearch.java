package com.example.user.employeemanagementsystem;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.employeemanagementsystem.api.provider.EmployeesProvider;
import com.example.user.employeemanagementsystem.recyclers.Division;
import com.example.user.employeemanagementsystem.recyclers.Employee;
import com.example.user.employeemanagementsystem.recyclers.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.realm.Realm;

public class EmployeeSearch extends DialogFragment implements OnClickListener {

    private TextView searchName;
    private TextView searchLastName;
    private TextView titleSearch;
    private CheckBox searchActive;
    private CheckBox searchInActive;
    private boolean active;
    private boolean inactive;
    private String first_name = null;
    private String last_name = null;
    private Integer division_id = null;
    private Button okBtn;
    private Button cancellBtn;
    private RecyclerViewAdapter adapter;
    final String LOG_TAG = "myLogs";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_finde_emloyee, null);
        okBtn = v.findViewById(R.id.search_btnYes);
        cancellBtn = v.findViewById(R.id.search_btnNo);
        okBtn.setOnClickListener(this);

        ArrayList<String> spinnerList = new ArrayList<String>();
        spinnerList.add("All division");
        List<Division> listDiv = Realm.getDefaultInstance().where(Division.class).findAll();
        for (Division division : listDiv) {
            spinnerList.add(division.getName());
        }

        // адаптер
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, spinnerList);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = (Spinner) v.findViewById(R.id.search_division);
        spinner.setAdapter(adapter1);
        // заголовок
        spinner.setPrompt("Divisions");
        // выделяем элемент
        spinner.setSelection(0);
        // устанавливаем обработчик нажатия
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                String s = (String) spinner.getSelectedItem();
                if (s.equals("All division")) {
                    division_id = null;
                } else {
                    division_id = (int) Realm.getDefaultInstance().where(Division.class).equalTo("name", s).findFirst().getId();
                    // показываем позиция нажатого элемента
                    Toast.makeText(getContext(), "Division = " + position, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        searchName = (TextView) v.findViewById(R.id.search_first_name);
        titleSearch = (TextView) v.findViewById(R.id.search_title);
        titleSearch.setText("Employee search");
        searchLastName = (TextView) v.findViewById(R.id.search_last_name);
        searchActive = (CheckBox) v.findViewById(R.id.search_check_active);
        searchInActive = (CheckBox) v.findViewById(R.id.search_check_inactive);

        searchActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    active = true;
                    searchInActive.setChecked(false);
                } else {
                    active = false;
                }
            }
        });

        searchInActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    inactive = true;
                } else {
                    inactive = false;
                }
            }
        });


        cancellBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return v;
    }

    UserSearch us = new UserSearch();


    public void onClick(View v) {
        first_name = searchName.getText().toString();
        last_name = searchLastName.getText().toString();
        EmployeesProvider employeesProvider = new EmployeesProvider();
        Observable<List<Employee>> employeesObservable = employeesProvider.getEmployees(active, inactive, first_name, last_name, division_id);
        employeesObservable.subscribe(employees -> {
            adapter.setListEmployee(employees);
            us.setActive(active);
            us.setInactive(inactive);
            us.setFirst_name(first_name);
            us.setLast_name(last_name);
            us.setDivision_id(division_id);
            Realm.getDefaultInstance().executeTransaction(realm -> {
                realm.copyToRealmOrUpdate(us);
            });
//            if (getActivity() != null) { ActivityPageView ma = (ActivityPageView ) getActivity(); ma.setUserSearch(us); }
        });

        dismiss();
    }

    public UserSearch getUs() {
        return us;
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }

    public RecyclerViewAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerViewAdapter adapter) {
        this.adapter = adapter;
    }
}
