package com.example.user.employeemanagementsystem;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.employeemanagementsystem.api.provider.DivisionsProvider;
import com.example.user.employeemanagementsystem.recyclers.Division;
import com.example.user.employeemanagementsystem.recyclers.RecyclerViewAdapterDivision;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observable;

public class DivisionAdd extends DialogFragment implements OnClickListener {

    private EditText editDivision;
    private TextView titleEditDivision;
    private Button divEditOkBtn;
    private Button divEditCancellBtn;
    private String name = null;

    private RecyclerViewAdapterDivision adapter;

    final String LOG_TAG = "myLogs";


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.division_edit, null);
        divEditOkBtn = v.findViewById(R.id.div_ed_ok);
        divEditCancellBtn = v.findViewById(R.id.div_ed_cancel);
        divEditOkBtn.setOnClickListener(this);


        editDivision = (EditText) v.findViewById(R.id.div_edit);
        titleEditDivision = (TextView) v.findViewById(R.id.div_ed_title);
        titleEditDivision.setText("Division edit");


        divEditCancellBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        return v;
    }


    public void onClick(View v) {
        name = editDivision.getText().toString();

        Pattern pattern = Pattern.compile("[A-Za-zА-Яа-я0-9'-]*");
        Matcher matchName = pattern.matcher(name);
        if (matchName.matches()) {

            DivisionsProvider divProvider = new DivisionsProvider();
            Observable<Division> divisionObservable = divProvider.postDivision(name);
            divisionObservable.subscribe(employee -> {
                System.out.println(employee.getName());
                adapter.refreshData();
            });

            dismiss();
        } else {
            Toast.makeText(getContext(), "Wrong input  Please try again.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }

    public RecyclerViewAdapterDivision getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerViewAdapterDivision adapter) {
        this.adapter = adapter;
    }


}
