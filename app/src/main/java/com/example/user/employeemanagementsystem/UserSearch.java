package com.example.user.employeemanagementsystem;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by USER on 1/26/2018.
 */

public class UserSearch extends RealmObject {
    @PrimaryKey
    private String Key = "key_search";
    private boolean active;
    private boolean inactive;
    private String first_name;
    private String last_name;
    private Integer division_id;

    public UserSearch() {
    }

    public UserSearch(boolean active, boolean inactive, String first_name, String last_name, Integer division_id) {
        this.active = active;
        this.inactive = inactive;
        this.first_name = first_name;
        this.last_name = last_name;
        this.division_id = division_id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isInactive() {
        return inactive;
    }

    public void setInactive(boolean inactive) {
        this.inactive = inactive;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Integer getDivision_id() {
        return division_id;
    }

    public void setDivision_id(Integer division_id) {
        this.division_id = division_id;
    }
}
