package com.example.user.employeemanagementsystem.api.result;

import com.example.user.employeemanagementsystem.recyclers.Employee;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 1/10/2018.
 */

public class EmployeeResult {
    @SerializedName("employee")
    private Employee employee;

    public EmployeeResult(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
