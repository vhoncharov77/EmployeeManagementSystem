package com.example.user.employeemanagementsystem;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.employeemanagementsystem.api.provider.EmployeesProvider;
import com.example.user.employeemanagementsystem.recyclers.Division;
import com.example.user.employeemanagementsystem.recyclers.Employee;
import com.example.user.employeemanagementsystem.recyclers.RecyclerViewAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.Observable;
import io.realm.Realm;

public class EmployeeEdit extends DialogFragment implements OnClickListener {

    private EditText editName;
    private EditText editLastName;
    private TextView titleEdit;
    private EditText editSalary;
    private TextView editDate;
    private CheckBox editActive;
    private CheckBox editInActive;
    private long id;
    private boolean active;
    private boolean inactive;
    private String birthdate;
    private String salary = null;
    private String first_name = null;
    private String last_name = null;
    private Integer division_id;
    private Button editOkBtn;
    private Button editCancellBtn;
    private Button datePicker;
    private int position;

    public void setPosition(int position) {
        this.position = position;
    }

    private RecyclerViewAdapter adapter;
    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    final String LOG_TAG = "myLogs";

    private Employee employee = new Employee();

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_edit_emloyee, null);
        editOkBtn = v.findViewById(R.id.edit_ok);
        editCancellBtn = v.findViewById(R.id.edit_cancel);
        datePicker = v.findViewById(R.id.edit_date_btn);
        editOkBtn.setOnClickListener(this);

        ArrayList<String> spinnerList = new ArrayList<String>();
        List<Division> listDiv = Realm.getDefaultInstance().where(Division.class).findAll();
        for (Division division : listDiv) {
            spinnerList.add(division.getName());
        }


        // адаптер
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, spinnerList);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = (Spinner) v.findViewById(R.id.edit_division);
        spinner.setAdapter(adapter2);
        // заголовок
        spinner.setPrompt("Divisions");
        // выделяем элемент
        spinner.setSelection(spinnerList.indexOf(employee.getDivisionname()));
        // устанавливаем обработчик нажатия
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                String s = (String) spinner.getSelectedItem();
                division_id = (int) Realm.getDefaultInstance().where(Division.class).equalTo("name", s).findFirst().getId();
                // показываем позиция нажатого элемента
                Toast.makeText(getContext(), "Division = " + division_id, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        editName = (EditText) v.findViewById(R.id.edit_first_name);
        titleEdit = (TextView) v.findViewById(R.id.edit_title);
        editSalary = (EditText) v.findViewById(R.id.edit_salary);
        editDate = (TextView) v.findViewById(R.id.edit_tvDate);
        titleEdit.setText("Employee edit");
        editLastName = (EditText) v.findViewById(R.id.edit_last_name);
        editActive = (CheckBox) v.findViewById(R.id.checkBox_edit_active);
        editInActive = (CheckBox) v.findViewById(R.id.checkBox_edit_inactive);

        editName.setText(employee.getFirstName());
        editLastName.setText(employee.getLastName());
        editSalary.setText(employee.getSalary());

        editDate.setText(formatter.format(employee.getBirthdate()));


        if (employee.isActive()) {
            editActive.setChecked(true);
            editInActive.setChecked(false);
            active = true;
        } else {
            editInActive.setChecked(true);
            editActive.setChecked(false);
            active = false;
        }

        editActive.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    active = true;
                    editInActive.setChecked(false);
                } else {
                    active = false;
                }

            }
        });

        editInActive.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    inactive = true;
                    editActive.setChecked(false);
                } else {
                    inactive = false;
                }
            }
        });


        editCancellBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        datePicker.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePicker dateDialog = new DatePicker();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                dateDialog.setDate(formatter.format(employee.getBirthdate()));
                dateDialog.setTextView(editDate);
                dateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });


        return v;
    }

    public void onClick(View v) {
        id = employee.getId();
        first_name = editName.getText().toString();
        last_name = editLastName.getText().toString();
        birthdate = editDate.getText().toString();
        salary = editSalary.getText().toString();

        Pattern pattern = Pattern.compile("[A-Za-zА-Яа-я0-9'-]*");
        Pattern patternNam = Pattern.compile("\\-?\\d+(\\.\\d{0,})?");
        Matcher matchSalary = patternNam.matcher(salary);
        Matcher matchName = pattern.matcher(first_name);
        Matcher matchLastName = pattern.matcher(last_name);
        if (matchName.matches() && matchLastName.matches() && matchSalary.matches()) {

            EmployeesProvider employeesProvider = new EmployeesProvider();
            Observable<Employee> employeeObservable = employeesProvider.editEmployee(id, active, first_name, last_name, birthdate, salary, division_id);
            employeeObservable.subscribe(employee -> {
                System.out.println(employee.getFirstName());
                adapter.refreshData();

            });
            dismiss();
        } else {
            Toast.makeText(getContext(), "Wrong input  Please try again.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }

    public RecyclerViewAdapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerViewAdapter adapter) {
        this.adapter = adapter;
    }
}
