package com.example.user.employeemanagementsystem.api.result;

import com.example.user.employeemanagementsystem.User;
import com.google.gson.annotations.SerializedName;

/**
 * Created by USER on 1/17/2018.
 */

public class UserResult {
    @SerializedName("user")
    private User user;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
